## Packages

- npm i react react-dom --save-dev
- npm i @babel/core babel-loader @babel/preset-env @babel/preset-react --save-dev
- npm i webpack webpack-cli --save-dev
- npm i css-loader --save-dev
- npm i redux react-redux --save-dev
- npm i mini-css-extract-plugin --save-dev
- npm i express
- npm i hbs
- npm i nodemon --save-dev

## Files
- Create .babelrc
- Create webpack.config.js