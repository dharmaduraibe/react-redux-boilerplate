
export const validate = content => ({
    type: 'USER_VALIDATE',
    isValid: true
});

export const createUser = data => ({
    type: 'USER_CREATE',
    payload: data
});
