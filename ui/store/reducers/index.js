import { combineReducers } from 'redux';
import userForm from './user_form';

export default combineReducers({ userForm });
