const initialState = {
    isValid: false,
    userData: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case 'USER_VALIDATE': {
            return {
                ...state,
                isValid: action.isValid
            };
        }
        case 'USER_CREATE': {
            const { payload: userData } = action;
            return {
                ...state,
                userData
            };
        }
        default:
            return state;
    }
}
