import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import store from './store'

import UserForm from './components/user_form'

const rootElement = document.getElementById('root')
ReactDOM.render(
    <Provider store={store}>
        <UserForm />
    </Provider>,
    rootElement
);
