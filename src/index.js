const express = require('express');
const hbs = require('hbs');
const path = require('path');
const configureRouters = require('./controllers');

const app = express();

app.set('views', path.resolve(__dirname, './views'));
app.use('/public', express.static(path.resolve(__dirname, '../public')));
app.set('view engine', 'html');
app.engine('html', hbs.__express);

configureRouters(app);

app.listen(8080, (err) => {
    if (err) {
        console.log(`Error in starting the app`, err);
    } else {
        console.log(`App is running in port ${8080}`);
    }
});
